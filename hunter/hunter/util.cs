﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hunter
{
    class util
    {
        private static log4net.ILog log = log4net.LogManager.GetLogger("MyLogger");

        public static void INFO(string str)
        {
            log.Info(str);
        }

        public static void INFOF(string format, params object[] args)
        {
            log.InfoFormat(format, args);
        }

        public static void ERROR(string str)
        {
            log.Error(str);
        }

        public static void ERRORF(string format, params object[] args)
        {
            log.ErrorFormat(format, args);
        }
    }
}
