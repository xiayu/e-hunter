﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace hunter
{
    class ExHentaiParse
    {
        public async void DownloadAllImage(string url)
        {
            var docIndex = await GetDocIndexUrl(url);
            var nextUrl = docIndex.IndexUrl;
            for (var i = 1; i < docIndex.DocLength; i++)
            {
                nextUrl = await GetImageUrl(nextUrl, i);
                if (nextUrl == "")
                {
                    break;
                }   
            }
        }

        public async Task<string> GetImageUrl(string url, int index)
        {
            string html = "";
            try
            {
                var client = new ExHttpClient();
                html = await client.GetHtmlString(url);
                var doc = new HtmlDocument();

                doc.LoadHtml(html);
                var imgNode = doc.DocumentNode.SelectSingleNode("//img[contains(@id, 'img')]");
                var imgUrl = imgNode.Attributes["src"].Value;
                using (var c = new WebClient())
                {
                    try
                    {
                        c.DownloadFileAsync(new Uri(imgUrl), index.ToString() + ".jpg");
                        c.DownloadFileCompleted += DonwloadFileSuccessed;
                    }
                    catch (Exception e)
                    {
                        util.ERROR("download url failed: " + imgUrl);
                    }
                }

                var nextNode = doc.DocumentNode.SelectSingleNode("//a[contains(@id, 'next')]");
                var nextUrl = nextNode.Attributes["href"].Value;
                return nextUrl;
            }
            catch (Exception e)
            {
                util.ERROR("error:" + e.ToString() + "||" + "url:" + url + "||" + "html:" + html);
                return null;
            }
            return null;
        }

        private void DonwloadFileSuccessed(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            string fileIdentifier = ((System.Net.WebClient)(sender)).QueryString["file"];
            util.INFO("download: " + fileIdentifier + " success");
        }

        public async Task<DocumentIndex> GetDocIndexUrl(string url)
        {
            string html = "";
            try
            {
                var res = new DocumentIndex();
                var client = new ExHttpClient();
                html = await client.GetHtmlString(url);
                var doc = new HtmlDocument();
                doc.LoadHtml(html);
                var name = doc.DocumentNode.SelectSingleNode("//h1[contains(@id, 'gn')]");
                res.Name = name.InnerText;

                var lengthNode = doc.DocumentNode.SelectNodes("//td[contains(@class, 'gdt2')]");
                res.DocLength = Convert.ToInt32(lengthNode[5].InnerText.Replace(" pages", ""));

                var gdtNode = doc.DocumentNode.SelectSingleNode("//div[contains(@id,'gdt')]");
                if (gdtNode == null)
                {
                    throw new Exception("parse gdt is error");
                }
                else
                {
                    var items = gdtNode.SelectNodes(".//a");
                    if (items == null)
                    {
                        throw new Exception("parse a is error");
                    }
                    else
                    {
                        var item = items[0];
                        res.IndexUrl = item.Attributes["href"].Value;
                        return res;
                    }
                }
            }
            catch (Exception e)
            {
                util.ERROR("error:" + e.ToString() +"||"+"url:" + url + "||" + "html:" + html);
                return null;
            }
            return null;
        }
    }
}
