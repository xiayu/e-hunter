﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace hunter_console
{
    class ExHentaiParse
    {
        public string DownloadAllImage(string url)
        {
            var docIndex = GetDocIndexUrl(url).Result;
            if (docIndex == null)
            {
                return "";
            }
            var nextUrl = docIndex.IndexUrl;
            for (var i = 1; i <= docIndex.DocLength; i++)
            {
                nextUrl = GetImageUrl(nextUrl, i, docIndex.Name);
                if (nextUrl == "")
                {
                    break;
                }   
            }
            Console.WriteLine("download " + url + " finish");
            return docIndex.Name;
        }

        private bool DownloadImageFile(WebClient client, string url, int index, string fileName)
        {
            try
            {
                util.INFO("download image:" + url);
                client.DownloadFile(new Uri(url), fileName + "/"+index.ToString().PadLeft(3, '0') + ".jpg");
            }
            catch
            {
                util.ERROR("download url failed: " + url);
                return false;
            }
            return true;
        }

        public string GetImageUrl(string url, int index, string fileName)
        {
            try
            {
                var client = new ExHttpClient();
                var html = client.GetHtmlString(url);
                var doc = new HtmlDocument();

                doc.LoadHtml(html.Result);
                var imgNode = doc.DocumentNode.SelectSingleNode("//img[contains(@id, 'img')]");
                var imgUrl = imgNode.Attributes["src"].Value;
                using (var c = new WebClient())
                {
                    for (int i = 0; i < 3; i++)
                    {
                        var flag = DownloadImageFile(c, imgUrl, index, fileName);
                        if (flag == true)
                        {
                            break;
                        }
                        util.INFO("retry url:" + imgUrl);
                    }
                }

                var nextNode = doc.DocumentNode.SelectSingleNode("//a[contains(@id, 'next')]");
                var nextUrl = nextNode.Attributes["href"].Value;
                return nextUrl;
            }
            catch (Exception e)
            {
                util.ERROR("error:" + e.ToString() + "||" + "url:" + url);
                return null;
            }
        }

        private string replaceInvalPath(string filename)
        {
            string str = filename;
            str = str.Replace("\\", string.Empty);
            str = str.Replace("/", string.Empty);
            str = str.Replace(":", string.Empty);
            str = str.Replace("*", string.Empty);
            str = str.Replace("?", string.Empty);
            str = str.Replace("\"", string.Empty);
            str = str.Replace("<", string.Empty);
            str = str.Replace(">", string.Empty);
            str = str.Replace("|", string.Empty);
            str = str.Replace(" ", string.Empty); 
            return str;
        }

        public async Task<DocumentIndex> GetDocIndexUrl(string url)
        {
            string html = "";
            try
            {
                var res = new DocumentIndex();
                var client = new ExHttpClient();
                html = await client.GetHtmlString(url);
                var doc = new HtmlDocument();
                doc.LoadHtml(html);
                var name = doc.DocumentNode.SelectSingleNode("//h1[contains(@id, 'gn')]");
                res.Name = replaceInvalPath(name.InnerText);

                if (Directory.Exists(res.Name) == false)
                {
                    Directory.CreateDirectory(res.Name);
                }
                else
                {
                    return null;
                }
                var lengthNode = doc.DocumentNode.SelectNodes("//td[contains(@class, 'gdt2')]");
                res.DocLength = Convert.ToInt32(lengthNode[5].InnerText.Replace(" pages", ""));

                var gdtNode = doc.DocumentNode.SelectSingleNode("//div[contains(@id,'gdt')]");
                if (gdtNode == null)
                {
                    throw new Exception("parse gdt is error");
                }
                else
                {
                    var items = gdtNode.SelectNodes(".//a");
                    if (items == null)
                    {
                        throw new Exception("parse a is error");
                    }
                    else
                    {
                        var item = items[0];
                        res.IndexUrl = item.Attributes["href"].Value;
                        return res;
                    }
                }
            }
            catch (Exception e)
            {
                util.ERROR("error:" + e.ToString() +"||"+"url:" + url + "||" + "html:" + html);
                return null;
            }
        }
    }
}
