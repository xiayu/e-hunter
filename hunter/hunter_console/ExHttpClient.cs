﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace hunter_console
{
    class ExHttpClient
    {
        public async Task<string> GetHtmlString(string url)
        {
            var handler = new HttpClientHandler() { UseCookies = false };
            var client = new HttpClient(handler);
            var message = new HttpRequestMessage(HttpMethod.Get, url);
            message.Headers.Add("Cookie",
                "s=b45e295dc; sk=qa39ckj1n3z9x7m59yskzpmgnh6d; ipb_pass_hash=c53281d74b22c2eb675872d7a800a2cb; ipb_member_id=3552014; yay=0; igneous=c550a3bc1; lv=1518706659-1520076946");

            var task = await client.SendAsync(message);

            var task2 = await task.Content.ReadAsStringAsync();

            client.Dispose();

            return task2;
        }
    }
}
