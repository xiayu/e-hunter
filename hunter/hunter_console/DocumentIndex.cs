﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hunter_console
{
    class DocumentIndex
    {
        public string IndexUrl { get; set; }
        public int DocLength { get; set; }
        public string Name { get; set; }
    }
}
