﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hunter_console
{
    class util
    {

        public static void INFO(string str)
        {
            Console.WriteLine(str);
        }

        public static void INFOF(string format, params object[] args)
        {
            Console.WriteLine(format, args);
        }

        public static void ERROR(string str)
        {
            Console.WriteLine(str);
        }

        public static void ERRORF(string format, params object[] args)
        {
            Console.WriteLine(format, args);
        }
    }
}
